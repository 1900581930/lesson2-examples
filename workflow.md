# Workflow

1. Prepossessing stage: g++ -E main.cpp > target/hello_world.ii
2. Compile only; do not assemble or link: g++ -S target/hello_world.ii -o target/hello_world.s
3. Assembly stage: as -o target/hello_world.o target/hello_world.s
4. Linking: g++ -o target/hello_world target/hello_world
5. Run: ./target/hello_world
6. Enjoy

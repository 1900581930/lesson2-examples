//
// Created by marsofandrew on 24.02.2020.
//

#include "student.hpp"

Student::Student(int age) :
        age_(age) {}

int Student::getAge() const {
    return age_;
}


#define sum(a, b) a + b

#include <iostream>

#include "student.hpp"

int main() {
  int test = 5 * sum(3, 8);
  std::cout << "test = " << test << "\n";

  int a = 5;
  Student student(6);
  int b = a + student.getAge();

  std::cout << "b = " << b << "\n";

  return 0;
}


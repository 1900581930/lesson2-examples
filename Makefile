COMPILER=g++
TARGET_DIR=target

build: all

all:
	mkdir -p ${TARGET_DIR}
	${COMPILER} *.cpp *.hpp -o ${TARGET_DIR}/program

run:
	./${TARGET_DIR}/program

clean:
	rm -rf ${TARGET_DIR}

preprocessing:
	${COMPILER} -E main.cpp

